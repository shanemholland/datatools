package com.medsis.datatools;

import com.medsis.datatools.commands.ConvertDates;
import com.medsis.datatools.commands.Help;
import com.medsis.datatools.commands.Version;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class Main {

    public static void main(String[] args) {

        if(args.length > 0) {
            for (int i = 0; i >= 0 && i < args.length; i++) {
                // Go through arguments
                switch (args[i]) {
                    case "convertDate":
                        i = ConvertDates.convertDates(args,i);
                        break;

                    case "version":
                    case "-v":
                        i= Version.showVersion();
                        break;

                    case "help":
                    case "-h":
                    case "usage":
                    case "-?":
                        i= Help.showHelp();
                        break;

                    case "all":
                    default:
                        showError(args[i]);

                }
            }
        }
    }

    private static String getInputString( String prompt) throws IOException {
        BufferedReader in = new BufferedReader
                ( new InputStreamReader( System.in )  );

        System.out.print (prompt);
        return in.readLine();
    }

    private static void showError(String command) {
        System.out.println("'" + command + "' is not a registered command. See 'medsisDataTools help'");
    }

}
