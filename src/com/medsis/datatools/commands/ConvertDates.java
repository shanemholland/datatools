package com.medsis.datatools.commands;

import com.medsis.datatools.converters.MS_SqlDataConverter;

public class ConvertDates {

    public static int convertDates(String[] args, Integer i) {
        if (args.length > i+2 && args[i+1].equals("-mssql") ) {
            if (args.length > i+3) {
                MS_SqlDataConverter.convertMsSqlDates(args[i+2], args[i+3]);
                i=i+3;
            } else {
                MS_SqlDataConverter.convertMsSqlDates(args[i+2]);
                i=i+2;
            }
        }
        return -2;
    }
}
