package com.medsis.datatools.commands;

import com.medsis.datatools.converters.AnsiCodes;

public class Help {

    public static int showHelp() {
        System.out.println("medsisDataTools.exe version " + Version.getVersion() + ", " + Version.getVendor());
        System.out.println("Usage: medsisDataTools.exe command [arguments...] [command options]");
        System.out.println("");

        // Version
        System.out.println(AnsiCodes.ANSI_YELLOW + "Version" + AnsiCodes.ANSI_WHITE);
        System.out.println("\tversion, -v \t\t\t\tDisplays Version and Vendor.");
        System.out.println("");

        // Help
        System.out.println(AnsiCodes.ANSI_YELLOW + "Help" + AnsiCodes.ANSI_WHITE);
        System.out.println("\thelp, -h, usage, -? \t\t\tDisplays helpful information about the application and its commands.");
        System.out.println("");

        // Convert Data
        System.out.println(AnsiCodes.ANSI_YELLOW + "Data Conversion" + AnsiCodes.ANSI_WHITE);
        System.out.println("\tconvertDates \t\t\t\tConverts dates in an SQL file for use in mySql.");
        System.out.println("\t\t\t\t\t\tAccepts -[dbtype] [inputFile] [(optional)outputFile]");
        System.out.println("\t\t\t\t\t\tex.) medsisDataTools.exe -mssql MSSQLDATA.sql MySQLDATA.sql");
        System.out.println("");

        return -2;
    }

}
