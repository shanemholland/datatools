package com.medsis.datatools.commands;

public class Version {

    public static int showVersion() {
        displayPackageDetails(Package.getPackage("com.medsis.datatools"));
        return -2;
    }

    public static String getVersion() {
        return Package.getPackage("com.medsis.datatools").getSpecificationVersion() + "." + Package.getPackage("com.medsis.datatools").getImplementationVersion();
    }

    public static String getVendor() {
        return Package.getPackage("com.medsis.datatools").getSpecificationVendor();
    }

    /**
     * Display (to standard output) package details for provided Package.
     *
     * @param pkg Package whose details need to be printed to standard output.
     */
    private static void displayPackageDetails(final Package pkg)
    {
        System.out.println(pkg.getSpecificationTitle());
        System.out.println("\tVersion: " + getVersion());
        System.out.println("\tVendor: " +  pkg.getSpecificationVendor());
    }
}
