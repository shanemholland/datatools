package com.medsis.datatools.converters;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class MS_SqlDataConverter {

    public static void convertMsSqlDates(String inputFile, String outputFile) {

        PrintWriter out = null;
        Path inPath = Paths.get(inputFile);

        try {
            Scanner scanner = new Scanner(inPath, StandardCharsets.UTF_8.name());
            out = new PrintWriter (outputFile);

            while (scanner.hasNextLine()) {
                out.println(convertMsSqlLineHexToDateTime(scanner.nextLine()));
            }

        } catch(Exception ex) {
            System.out.println( AnsiCodes.ANSI_RED + "FAILED!!!" );
            System.out.println( AnsiCodes.ANSI_WHITE + "Error while converting file: ");
            System.out.println( AnsiCodes.ANSI_RED + ex.getLocalizedMessage() + AnsiCodes.ANSI_WHITE );
            ex.printStackTrace(System.out);
        }
        finally{
            try {
                if (out != null) {
                    out.close();
                }
            } catch (Exception ex) {
                System.out.println( AnsiCodes.ANSI_RED + "FAILED!!!" );
                System.out.println( AnsiCodes.ANSI_WHITE + "Error while closing file: ");
                System.out.println( AnsiCodes.ANSI_RED + ex.getLocalizedMessage() + AnsiCodes.ANSI_WHITE );
                ex.printStackTrace(System.out);
            }
        }
    }

    public static void convertMsSqlDates(String inputFile) {

        Path inPath = Paths.get(inputFile);

        try {
            Scanner scanner = new Scanner(inPath, StandardCharsets.UTF_8.name());

            while (scanner.hasNextLine()) {
                System.out.println(convertMsSqlLineHexToDateTime(scanner.nextLine()));
            }

        } catch(Exception ex) {
            System.out.println( AnsiCodes.ANSI_RED + "FAILED!!!" );
            System.out.println( AnsiCodes.ANSI_WHITE + "Error while converting file: ");
            System.out.println( AnsiCodes.ANSI_RED + ex.getLocalizedMessage() + AnsiCodes.ANSI_WHITE );
            ex.printStackTrace(System.out);
        }
    }

    private static String convertMsSqlLineHexToDateTime(String line) {
        String result = line;
        Matcher subMatcher =  Pattern.compile("(0x([A-Fa-f0-9]){8,16}?)\\b").matcher(line);

        boolean found = subMatcher.find();

        while (found) {
            String match = line.substring(subMatcher.start(), subMatcher.end());
            String nDate = convertMsSqlHexToDateTime(match);

            result = result.replace( match, "'" + nDate + "'");
            found = subMatcher.find();
        }

        return result;
    }



    private static String convertMsSqlHexToDateTime(String hexSqlDate) {
        String result;
        LocalDate date = LocalDate.of(2001, 1, 1);
        LocalTime time = LocalTime.of(0,0,0);

        if (hexSqlDate.replace("0x","").length() > 8) {
            // This is a DateTime and should be treated as such
            // Take the first 8 hex digits and convert to decimal.  This is days since 1900-1-1
            Integer addDays = parseInt(hexSqlDate.replace("0x","").substring(0,8),16);
            // Take the last 8 hex digits and convert to decimal.  This is ticks (1/300th of a second)
            Integer addSeconds = Math.round(parseInt(hexSqlDate.replace("0x","").substring(8,16),16)/300);

            date = date.plusDays(addDays);
            date = date.minusYears(101);
            time = time.plusSeconds(addSeconds);

            result = date.toString() + " " + time.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
        } else {
            // This is just a date, without time
            Long addDays;
            String addDaysBad = hexSqlDate.replace("0x","");
            // Rearrange the hex to the correct format
            String addDaysGood = addDaysBad.substring(6,8) + addDaysBad.substring(4,6) + addDaysBad.substring(2,4) + addDaysBad.substring(0,2);
            // Interperet Integer value of rearranged hex string.  This is days since 1-1-1
            addDays = Long.parseLong(addDaysGood,16);

            date = date.plusDays(addDays);
            date = date.minusYears(2000);

            result = date.toString();
        }
        return result;
    }
}
